/*
 * Author Alec Shewell
 */

package celsius;

public class Celsius {
	public static int fromFahrenheit(int temp) {
		return (int) ((5 *(temp - 32)) / 9.0);
	}

}
