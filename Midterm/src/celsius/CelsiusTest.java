/*
 * Author Alec Shewell
 */

package celsius;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void TestFromFahrenheitRegular() {
		assertTrue("Has not yet been implemented", Celsius.fromFahrenheit(41) == 5);
	}
	@Test
	public void TestFromFahrenheitBoundaryIn() {
		assertTrue("Has not yet been implemented", Celsius.fromFahrenheit(45) == 7);
	}
	@Test
	public void TestFromFahrenheitBoundaryOut() {
		assertFalse("Has not yet been implemented", Celsius.fromFahrenheit(45) == 6);
	}
	@Test
	public void TestFromFahrenheitException() {
		assertFalse("Has not yet been implemented", Celsius.fromFahrenheit(-43) == 3);
	}
}
